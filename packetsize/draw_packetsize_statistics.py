"""
draw_packetsize_statistics.py
"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time

class draw_packetsize(object):

	def __init__(self, t5, t5_size, t10, t10_size, 
			t20, t20_size, t50, t50_size):
		self.__T5 = t5
		self.__sizeT5 = t5_size
		self.__T10 = t10
		self.__sizeT10 = t10_size
		self.__T20 = t20
		self.__sizeT20 = t20_size
		self.__T50 = t50
		self.__sizeT50 = t50_size
		"END"

	def draw_Graph_combine(self, size_t5, throughput_t5, 
			size_t10, throughput_t10, 
			size_t20, throughput_t20, 
			size_t50, throughput_t50):
		filename = "PacketSize_combine.png"

		"Line Graph - Values"
		point_t5 = plt.plot(size_t5, throughput_t5, 'ro')
		point_t10 = plt.plot(size_t10, throughput_t10, 'bo')
		point_t20 = plt.plot(size_t20, throughput_t20, 'go')
		point_t50 = plt.plot(size_t50, throughput_t50, 'yo')
		"Help Line"
		plt.plot(size_t5, throughput_t5, 'r--')
		plt.plot(size_t10, throughput_t10, 'b--')
		plt.plot(size_t20, throughput_t20, 'g--')
		plt.plot(size_t50, throughput_t50, 'y--')
		"Grid"
		plt.grid(True)
		"Labels"
		plt.xlabel("Packet Size")
		plt.ylabel("Avg. Throughput")
		plt.legend( (point_t5[0], point_t10[0], point_t20[0], 
			point_t50[0]), ('t5', 't10', 't20', 't50'),
			loc='upper center', bbox_to_anchor=(0.5, 1.12),
			fancybox=True, shadow=True, ncol=4)
		"Save Graph"
		plt.savefig(filename)
		"END"

	def draw_Graph(self, sizes, throughput, name):
		filename = "PacketSize_" + name + ".png"

		fig = plt.figure(name)
		"Line Graph - Values"
		point = plt.plot(sizes, throughput, 'ro')
		"Help Line"
		plt.plot(sizes, throughput, 'r--')
		"Grid"
		plt.grid(True)
		"Labels"
		plt.xlabel("Packet Size")
		plt.ylabel("Avg. Throughput")
		"Save Graph"
		plt.savefig(filename)
		"END"


	def run(self):
		"Draw one by one"
		self.draw_Graph(self.__sizeT5, self.__T5, "t5")
		self.draw_Graph(self.__sizeT10, self.__T10, "t10")
		self.draw_Graph(self.__sizeT20, self.__T20, "t20")
		self.draw_Graph(self.__sizeT50, self.__T50, "t50")
		"Draw combine"
		self.draw_Graph_combine(self.__sizeT5, self.__T5, 
			self.__sizeT10, self.__T10, self.__sizeT20, 
			self.__T20, self.__sizeT50, self.__T50)
		print("Erfolgreich")
		"END"

if __name__ == '__main__':

	t5 = [14.44, 72.08, 148.47, 208.20]
	t5_size = [100, 500, 1000, 1450]
	t10 = [13.89, 68.16, 132.82, 181.71]
	t10_size = [100, 500, 1000, 1450]
	t20 = [13.26, 65.08, 126.11, 174.61]
	t20_size = [100, 500, 1000, 1450]
	t50 = [12.82, 63.10, 122.88, 173.71, 
		87.23, 92.97, 98.33, 104.96, 110.43]
	t50_size = [100, 500, 1000, 1450, 
		700, 750, 800, 850, 900]
	graph = draw_packetsize(t5, t5_size, t10, t10_size,
		t20, t20_size, t50, t50_size)
	graph.run()
	"END"