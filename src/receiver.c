#include <assert.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include "topp.h"

int32_t sock_init(int32_t *, uint16_t, char *, uint8_t, uint32_t);
void serve(int32_t *, char *, uint32_t);
void evaluate(struct msrmnt *, uint32_t, uint32_t);
void eval(struct msrmnt *, uint32_t, uint32_t);

int32_t main(int32_t argc, char *argv[]) {
	char iface[16] = {0};
	uint8_t args = 0;
	uint16_t port = 0;
	int32_t c = 1, option_index = 0, sock = 0;
	uint32_t timeout = 0;
	uint32_t trains = 0;
	struct option long_options[] = {
		{"bind", required_argument, 0, 'b'},
		{"port", required_argument, 0, 'p'},
		{"timeout", required_argument, 0, 't'},
		{"trains", required_argument, 0, 'z'}
	};

	while ((c = getopt_long(argc, argv, "b:p:t:z:", long_options,
					&option_index)) != -1) {
		switch (c) {
			case 'b':
				strncpy(iface, optarg, sizeof(iface));
				args++;
				break;
			case 'p':
				port = (uint16_t) atoi(optarg);
				args++;
				break;
			case 't':
				timeout = (uint32_t) atoi(optarg);
				args++;
				break;
			case 'z':
				trains = (int) atoi(optarg);
				args++;
				break;
			default:
				fprintf(stderr, "usage: %s "
						"{-b|--bind} <interface> "
						"{-p|--port} <port> "
						"{-t|--timeout} <timeout> "
						"{-z|--trains}\n", argv[0]);
				return 1;
		}
	}

	// getopt(3) doesn't support mandatory options
	if (args != 4) {
		fprintf(stderr, "usage: %s "
				"{-b|--bind} <interface> "
				"{-p|--port} <port> "
				"{-t|--timeout} <timeout> "
				"{-z|--trains}\n", argv[0]);
		return 1;
	}

	if (sock_init(&sock, port, iface, 4, timeout) == 0) {
		serve(&sock, iface, trains);
	}
	return 0;
}

int32_t sock_init(int32_t *sock, uint16_t port, char iface[16], uint8_t ipv,
		uint32_t timeout) {
	void *sin;
	size_t sin_sz;
	struct timeval tv_timeout = {.tv_sec = timeout, .tv_usec = 0};
	struct sockaddr_in sin4 = {0};
	struct sockaddr_in6 sin6 = {0};

	switch (ipv) {
		case 4:
			sin4.sin_family = AF_INET;
			sin4.sin_port = htons(port);
			sin4.sin_addr.s_addr = htonl(INADDR_ANY);
			sin = &sin4;
			sin_sz = sizeof(sin4);
			*sock = socket(AF_INET, SOCK_DGRAM, 0);
			break;
		case 6:
			sin6.sin6_family = AF_INET6;
			sin6.sin6_port = htons(port);
			sin6.sin6_addr = in6addr_any;
			sin = &sin6;
			sin_sz = sizeof(sin6);
			*sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
			break;
		default:
			return -1;
	}

	if (*sock == -1) {
		perror("socket");
		return -1;
	}

	// see bind(2) and bind(3)
	if (bind(*sock, (struct sockaddr *) sin, sin_sz) == -1) {
		perror("bind");
		close(*sock);
		return -1;
	}

	// see setsockopt(2) and setsockopt(3)
	if (setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, &tv_timeout,
				sizeof(tv_timeout)) != 0) {
		perror("setsockopt");
		close(*sock);
		return -1;
	}

	// bind to interface
	if (setsockopt(*sock, SOL_SOCKET, SO_BINDTODEVICE, iface,
			sizeof(iface)) != 0) {
		perror("setsockopt");
		close(*sock);
		return -1;
	}
	return 0;
}

void serve(int32_t *sock, char *iface, uint32_t trains) {
	char *buffer = NULL;
	uint16_t iter = 0, k = 0;
	uint32_t mtu = 0;
	ssize_t msglen = 0;
	struct msg *msg = NULL;
	struct msrmnt rec[trains];
	struct ifreq ifr;

	// determine MTU of interface
	strcpy(ifr.ifr_name, iface);
	ioctl(*sock, SIOCGIFMTU, &ifr);
	mtu = ifr.ifr_mtu;

	buffer = malloc(mtu);
	if (buffer == NULL) {
		perror("malloc");
		close(*sock);
		return;
	}

	// initialize buffer
	memset(buffer, 0, mtu);

	msg = (struct msg *) buffer;

	// initialize records
	memset(rec, 0, sizeof(rec));

	fprintf(stderr, "Listening ...\n");

	for (;;) {
		msglen = recv(*sock, buffer, mtu, 0);
		if (msglen == -1) {
			perror("recv");
			free(buffer);
			close(*sock);
			return;
		}
		switch (msg->type) {
			case MSG_CTRL:
#ifdef NDEBUG
				fprintf(stderr, "[CTRL: %ld] iter = %u, size = %u\n",
						msglen, msg->data.c.iter, msg->data.c.size);
#endif
				iter = msg->data.c.iter;
				break;
			case MSG_MSRMNT:
#ifdef NDEBUG
				fprintf(stderr, "[MSRMNT: %ld] iter = %u, id = %u, r_id = %u"
						"\n", msglen, htons(msg->data.m.iter), msg->data.m.id,
						msg->data.m.r_id);
#endif
				clock_gettime(CLOCK_MONOTONIC_RAW, &rec[k % trains].rtime);
				rec[k % trains].iter = htons(msg->data.m.iter);
				rec[k % trains].id = msg->data.m.id;
				rec[k % trains].otime = msg->data.m.otime;
				rec[k % trains].r_id = k % trains;
				k++;
				if (k % trains == 0) {
					// evaluate after each {TRAIN-LENGTH}th train
					eval(rec, trains, msglen);
				}
				if (k >= iter * trains) {
					close(*sock);
					free(buffer);
					return;
				}
				break;
			default:
				fprintf(stderr, "Unknown message type: %i\n", msg->type);
		}
	}
}

void eval(struct msrmnt *rec, uint32_t len, uint32_t size) {
	uint64_t ts_otime[2] = {0}, ts_rtime[2] = {2};
	double srate = 0, rrate = 0;

	for (uint32_t i = 0; i < len; i++) {
		if (rec[i].id != rec[i].r_id) {
			fprintf(stderr, "packages out of order\n");
			return;
		}
	}
	ts_otime[0] = rec[0].otime.tv_sec * 1000000000 + rec[0].otime.tv_nsec;
	ts_otime[1] = rec[len-1].otime.tv_sec * 1000000000 +
		rec[len-1].otime.tv_nsec;
	ts_rtime[0] = rec[0].rtime.tv_sec * 1000000000 + rec[0].rtime.tv_nsec;
	ts_rtime[1] = rec[len-1].rtime.tv_sec * 1000000000 +
		rec[len-1].rtime.tv_nsec;
	srate = (double) size * len / (ts_otime[1] - ts_otime[0]) * 1000000000;
	rrate = (double) size * len / (ts_rtime[1] - ts_rtime[0]) * 1000000000;
	// ignore infinite rates
	if (ts_otime[0] != ts_rtime[1] && ts_rtime[0] != ts_rtime[1]) {
		fprintf(stdout, "sending @ %7.2f MB/s (%8.2f Mbit/s), "
				"receiving @ %7.2f MB/s (%8.2f Mbit/s), %+7.2f %%\n",
				srate / 1048576, srate / 1048576 * 8, rrate / 1048576,
				rrate / 1048576 * 8, (srate / rrate - 1) * 100);
	} else {
		fprintf(stderr, "Ignoring train due to infinite rate\n");
	}
}
