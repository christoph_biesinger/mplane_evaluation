#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#include "vps.h"

int main(int argc, char *argv[])
{
	char daddr[40] = {0};
	uint8_t args = 0;
	uint16_t packetsize = 0;
	int c = 1, option_index = 0;

	struct option long_options[] = {
		{"dest", required_argument, 0, 'd'},
		{"size", required_argument, 0, 's'}
	};

	while (( c = getopt_long(argc, argv, "d:s:", long_options, 
					&option_index)) != -1) {
		switch (c) {
			case 'd':
				strncpy(daddr, optarg, sizeof(daddr));
				args++;
				break;
			case 's':
				packetsize = (uint16_t) atoi(optarg);
				args++;
				break;
			default:
				fprintf(stderr, "usage: %s"
						"{-d|--dest} <destination> "
						"{-s|--size} <size>\n", argv[0]);
				return 1; 
		}
	}

	if (args != 2) {
		fprintf(stderr, "usage: %s"
						"{-d|--dest} <destination> "
						"{-s|--size} <size>\n", argv[0]);
				return 1; 
	}

	main_loop(daddr, packetsize);
	// if (argc > 1) {
	// 	main_loop(argv[1]);
	// }
}

void ping(int sock, uint16_t bufsz)
{
	char buf[bufsz];
	struct icmphdr *icmp = (struct icmphdr *) buf;
	// Put timestamp in the first 8 bytes of data
	struct timeval *tv = (struct timeval *) (buf + 8);

	memset(buf, 0, bufsz);

	gettimeofday(tv, NULL);
	icmp->type = ICMP_ECHO;
	icmp->un.echo.id = getpid();
	icmp->checksum = in_cksum((uint16_t *) buf, bufsz, 0);

	if (send(sock, buf, bufsz , 0) == -1) {
		perror("send");
		return;
	}
}

int pinger(int sock[2], uint16_t payload, char *recv_buf, int mtu) {
	int ret = 0;
	uint64_t time[10] = {0};
	struct ping p;

	for (int i = 0; i < 10;) {
		ping(sock[0], payload);
		if (recv(sock[1], recv_buf, mtu, 0) == -1) {
			return UNREACHABLE;
		}
		ret = parse_reply(recv_buf, &(p.dst), &p.tv[i]);
		time[i] = p.tv[i].tv_sec * 1e6 + p.tv[i].tv_usec;
		fprintf(stderr, ".");
		switch (ret) {
			case 0:
				i++;
				break;
			case DONE:
			case UNREACHABLE:
			case QUIT:
				return ret;
		}
		sleep(1);
	}

	fprintf(stderr, "\n");
	printf("dst %s, min %.2f, max %.2f, avg %.2f \n",
			inet_ntoa(*((struct in_addr *) p.dst)),
			min(time, 10) / 1e3,
			max(time, 10) / 1e3,
			avg(time, 10) / 1e3);

	return 0;
}

void main_loop(char dst[40], uint16_t packetsize)
{
	char *buf = NULL;
	int sock[2] = {0}, mtu = 0, ret = 0;
	socklen_t mtu_len = sizeof(mtu);
	struct sockaddr_in sin = {0}, sout = {0};
	struct timeval tv = {
		.tv_sec = 2,
		.tv_usec = 0
	};

	sock[0] = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sock[0] == -1) {
		perror("socket (send)");
		goto cleanup;
	
}
	sock[1] = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sock[1] == -1) {
		perror("socket (recv)");
		goto cleanup;
	}

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr(dst);

	sout.sin_family = AF_INET;
	sout.sin_addr.s_addr = htons(INADDR_ANY);

	if (bind(sock[1], (struct sockaddr *) &sout, sizeof(sout)) == -1) {
		perror("bind");
		goto cleanup;
	}

	if (connect(sock[0], (struct sockaddr *) &sin, sizeof(sin)) == -1) {
		perror("connect");
		goto cleanup;
	}

	getsockopt(sock[0], SOL_IP, IP_MTU, &mtu, &mtu_len);
	if (mtu == 0) {
		fprintf(stderr, "MTU is zero");
		goto cleanup;
	}

	buf = malloc(mtu);
	if (buf == NULL) {
		perror("malloc");
		goto cleanup;
	}
	// ttl
	for (int ttl = 1; ttl <= 30; ttl++) {
		if (setsockopt(sock[0], IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl)) == -1) {
			perror("sockopt IP_TTL");
			break;
		}
		if (setsockopt(sock[1], SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) == -1) {
			perror("sockopt SO_RCVTIMEO");
		}


		// payload
		printf("\nttl %d, payload %d, ", ttl, packetsize);
		ret = pinger(sock, packetsize, buf, mtu);
		if (ret == UNREACHABLE) {
			continue;
		} else if (ret == QUIT) {
			goto cleanup;
		}

		// for (int size = 100; size <= 1400; size += 100) {
		// 	printf("ttl %d, payload %d, ", ttl, size);
		// 	ret = pinger(sock, size, buf, mtu);
		// 	if (ret == UNREACHABLE) {
		// 		break;
		// 	} else if (ret == QUIT) {
		// 		goto cleanup;
		// 	}
		// }

		if (ret == DONE) {
			break;
		}
	}

cleanup:
	if (buf != NULL) {
		free(buf);
	}
	close(sock[0]);
	close(sock[1]);
}

int parse_reply(char *msg, uint32_t **pdst, struct timeval *ptv)
{
	int ret = 0;
	struct icmphdr *icmp_reply = (struct icmphdr *) (msg + 20);
	struct iphdr *ip_reply = (struct iphdr *) msg;
	struct icmphdr *icmp = NULL;
	struct timeval *tv = NULL;

	gettimeofday(ptv, NULL);

	switch (icmp_reply->type) {
		case 0:  // echo reply
			icmp = icmp_reply;
			tv = (struct timeval *) (msg + 28);
			ret = DONE;
			break;
		case 3:  // destination unreachable
			return UNREACHABLE;
		case 11:  // ttl exceeded
			icmp = (struct icmphdr *) (msg + 48);
			tv = (struct timeval *) (msg + 56);
			break;
		default:
			return QUIT;
	}

	if (getpid() != icmp->un.echo.id) {
		return NOTOURS;
	}

	tvsub(ptv, tv);
	*pdst = &(ip_reply->saddr);

	return ret;
}

#if BYTE_ORDER == LITTLE_ENDIAN
# define ODDBYTE(v)	(v)
#elif BYTE_ORDER == BIG_ENDIAN
# define ODDBYTE(v)	((u_short)(v) << 8)
#else
# define ODDBYTE(v)	htons((u_short)(v) << 8)
#endif

uint16_t in_cksum(const uint16_t *addr, int len, uint16_t csum)
{
	int nleft = len;
	const uint16_t *w = addr;
	uint16_t answer;
	int sum = csum;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1) {
		sum += ODDBYTE(*(uint8_t *) w); /* le16toh() may be unavailable on old systems */
	}

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */
	return answer;
}

static inline void tvsub(struct timeval *out, struct timeval *in)
{
	if ((out->tv_usec -= in->tv_usec) < 0) {
		--out->tv_sec;
		out->tv_usec += 1000000;
	}
	out->tv_sec -= in->tv_sec;
}

static inline uint64_t min(uint64_t *a, size_t sz) {
    uint64_t m = a[0];

    for (size_t i = 0; i < sz; i++) {
	    if (a[i] < m) {
		    m = a[i];
	    }
    }

    return m;
}

static inline uint64_t max(uint64_t *a, size_t sz) {
    uint64_t m = a[0];

    for (size_t i = 0; i < sz; i++) {
	    if (a[i] > m) {
		    m = a[i];
	    }
    }

    return m;
}

static inline double avg(uint64_t *a, size_t sz) {
	double m = 0;

	for (size_t i = 0; i < sz; i++) {
		m += a[i];
	}

	if (sz == 0) {
		return 0;
	} else {
		return m / sz;
	}
}
