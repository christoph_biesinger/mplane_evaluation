#ifndef VPS_H
#define VPS_H

enum reply {
	DONE = 1,
	NOTOURS,
	QUIT,
	UNREACHABLE,
};

struct ping {
	struct timeval tv[10];
	uint32_t *dst;
};

void ping(int sock, uint16_t bufsz);
int pinger(int sock[2], uint16_t payload, char *recv_buf, int mtu);
void main_loop(char dst[40], uint16_t packetsize);
int parse_reply(char *msg, uint32_t **pdst, struct timeval *ptv);
uint16_t in_cksum(const uint16_t *addr, int len, uint16_t csum);
static inline void tvsub(struct timeval *out, struct timeval *in);
static inline uint64_t min(uint64_t *a, size_t sz);
static inline uint64_t max(uint64_t *a, size_t sz);
static inline double avg(uint64_t *a, size_t sz);

#endif
