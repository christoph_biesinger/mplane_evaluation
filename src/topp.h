#ifndef TOPP_H
#define TOPP_H

#ifndef R_MIN
// 10 MB/s
#define R_MIN 10485760L
#endif
#ifndef R_MAX
// 250 MB/s
#define R_MAX 262144000L
#endif

#define TRAIN_LEN 2

enum msgtype {
	MSG_CTRL,
	MSG_MSRMNT
};

/*
 * to identify out-of-order packets a recv counter could be added
 */
struct msrmnt {
	uint16_t iter;  // train number
	uint8_t id;  // packet number (sender)
	uint8_t r_id;  // packet number (receiver)
	struct timespec otime;  // originate timestamp
	struct timespec rtime;  // received timestamp
} __attribute__((packed));

struct ctrl {
	uint16_t iter;
	uint16_t size;
} __attribute__((packed));

struct msg {
	enum msgtype type;
	union {
		struct msrmnt m;
		struct ctrl c;
	} data;
} __attribute__((packed));

#endif
