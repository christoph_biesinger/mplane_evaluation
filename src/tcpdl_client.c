#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#define handle_error(msg) \
	do {perror(msg); exit(EXIT_FAILURE);} while(0)

#define usage() \
	fprintf(stderr, "usage: %s " \
			"(-b|--bind) <interface> " \
			"(-d|--dest) <destination> " \
			"(-p|--port) <port>\n", argv[0])

#define tdiff(a, b) \
	((b.tv_sec * 1e9 + b.tv_nsec) - (a.tv_sec * 1e9 + a.tv_nsec))

struct sample {
	struct timespec ref;
	struct timespec ts;
	ssize_t bytes;
};

int client_sock_init(char*, uint16_t, char*);
int run(int);
int cmp_sample(const void *, const void *);

int main(int argc, char *argv[]) {
	char dest[40] = {0}, iface[16] = {0};
	int sock = 0;
	uint8_t args = 0;
	uint16_t port = 0;
	int32_t c = 1, option_index = 0;
	struct option long_options[] = {
		{"bind", required_argument, 0, 'b'},
		{"dest", required_argument, 0, 'd'},
		{"port", required_argument, 0, 'p'}
	};

	while ((c = getopt_long(argc, argv, "b:d:p:", long_options,
					&option_index)) != -1) {
		switch (c) {
			case 'b':
				strncpy(iface, optarg, sizeof(iface));
				args++;
				break;
			case 'd':
				strncpy(dest, optarg, sizeof(dest));
				args++;
				break;
			case 'p':
				port = (uint16_t) atoi(optarg);
				args++;
				break;
			default:
				usage();
				exit(1);
		}
	}

	if (args != 3) {
		usage();
		exit(1);
	}

	sock = client_sock_init(dest, port, iface);
	run(sock);
	close(sock);

	return EXIT_SUCCESS;
}

int client_sock_init(char dest[40], uint16_t port, char iface[16]) {
	int sock = 0;
	struct sockaddr_in addr = {0};

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(dest);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		handle_error("socket");
	}

	if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
		handle_error("connect");
	}
	setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, iface,
		sizeof(iface));

	return sock;
}

int run(int sock) {
	char *buf = NULL;
	socklen_t mtu_len = 0;
	size_t mtu = 0;
	int i = 0;
	double rate = 0;
	ssize_t bytes_received = 0, bytes_sent = 0, total = 0, probe = 0;
	struct timespec ts_start = {0}, ts_end = {0};
	struct sample samples[20];

	memset(samples, 0, sizeof(samples));

	// get MTU
	mtu_len = sizeof(mtu);
	getsockopt(sock, SOL_IP, IP_MTU, &mtu, &mtu_len);
	buf = malloc(mtu);
	if (buf == NULL) {
		close(sock);
		handle_error("malloc");
	}

	/* ignore initial ~500 kiB */
	while ((bytes_received = recv(sock, buf, mtu, 0)) > 0) {
		total += bytes_received;
		if (bytes_received == -1) {
			free(buf);
			close(sock);
			handle_error("recv");
		}
		/* 512000 size*/
		if ((size_t) total == (512000 / mtu) * mtu) {
			break;
		}
	}
	printf("Sending ACK for initial bytes...\n");
	strncpy(buf, "ACK", 4);
	bytes_sent = send(sock, buf, strlen(buf), 0);
	if (bytes_sent > 0) {
		printf("OK\n");
	} else if (bytes_sent == 0) {
		printf("Couldn't send ACK\n");
		free(buf);
		close(sock);
		exit(1);
	} else if (bytes_sent == -1) {
		free(buf);
		close(sock);
		handle_error("send");
	}

	total = 0;
	clock_gettime(CLOCK_MONOTONIC_RAW, &ts_start);
	while ((bytes_received = recv(sock, buf, mtu, 0)) > 0) {
		total += bytes_received;
		probe += bytes_received;
		/* 5 MB / 20 probes = 262144 bytes/probe 131072*/
		if ((size_t) probe > 131072 - mtu && (size_t) probe < 131072 + mtu) {
			clock_gettime(CLOCK_MONOTONIC_RAW, &samples[i].ts);
			samples[i].bytes = probe;
			samples[i].ref = i == 0 ? ts_start : samples[i-1].ts;
			probe = 0;
			i++;
		}
	}

	if (bytes_received == -1) {
		free(buf);
		close(sock);
		handle_error("recv");
	}
	clock_gettime(CLOCK_MONOTONIC_RAW, &ts_end);


	/*qsort(samples, sizeof(samples) / sizeof(samples[0]),
			sizeof(samples[0]), cmp_sample);
	*/
	/* ignore lowest 30% (6/20) and highest 10% (2/20) */
	// for (int i = 6, i < 18; i++)
	/* all data */
	for (int i = 0; i < 40; i++) {
		printf("[probe %02d] %zd bytes @ %.2f kiB\n",
				i,
				samples[i].bytes,
				samples[i].bytes /
				tdiff(samples[i].ref, samples[i].ts) * 976262.5);
	}

	rate = (double) total / tdiff(ts_start, ts_end) * 976562.5;
	printf("Bytes received: %zd @ %.2f kiB/s\n", total, rate);

	free(buf);
	return EXIT_SUCCESS;
}

int cmp_sample(const void *a, const void *b) {
	struct sample *spl_a = (struct sample *) a;
	struct sample *spl_b = (struct sample *) b;
	return tdiff(spl_b->ref, spl_b->ts) - tdiff(spl_a->ref, spl_a->ts);
}
