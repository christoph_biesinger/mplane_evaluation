#include <assert.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "topp.h"

int32_t sock_init(int32_t *, uint16_t, char *, char *, uint8_t);
int32_t set_params(int32_t *, uint16_t, uint16_t);
void send_train(int32_t *, uint16_t, uint16_t, uint32_t);

int32_t main(int32_t argc, char *argv[]) {
	char daddr[40] = {0}, iface[16] = {0};
	uint8_t args = 0;
	uint16_t iter = 0, port = 0, size = 0;
	uint32_t trains = 0;
	int32_t c = 1, option_index = 0, sock = 0;
	// TODO: add flags for IPv4 and IPv6
	struct option long_options[] = {
		{"bind", required_argument, 0, 'b'},
		{"dst", required_argument, 0, 'd'},
		{"iter", required_argument, 0, 'i'},
		{"port", required_argument, 0, 'p'},
		{"size", required_argument, 0, 's'},
		{"trains", required_argument, 0, 'z'}
	};

	while ((c = getopt_long(argc, argv, "b:d:i:p:s:z:", long_options,
					&option_index)) != -1) {
		switch (c) {
			case 'b':
				strncpy(iface, optarg, sizeof(iface));
				args++;
				break;
			case 'd':
				strncpy(daddr, optarg, sizeof(daddr));
				args++;
				break;
			case 'i':
				iter = (uint16_t) atoi(optarg);
				args++;
				break;
			case 'p':
				port = (uint16_t) atoi(optarg);
				args++;
				break;
			case 's':
				size = (uint16_t) atoi(optarg);
				args++;
				break;
			case 'z':
				trains = (int) atoi(optarg);
				args++;
				break;
			default:
				fprintf(stderr, "usage: %s "
						"{-b|--bind} <interface> "
						"{-d|--dst} <dest_ip> "
						"{-p|--port} <port> "
						"{-i|--iter} <iterations> "
						"{-s|--size} <packet_size> "
						"{-z|--trains} <train_length>\n", argv[0]);
				return 1;
		}
	}

	printf("parameter: %d", args);
	// getopt(3) doesn't support mandatory options
	if (args != 6) {
		fprintf(stderr, "usage: %s "
				"{-b|--bind} <interface> "
				"{-d|--dst} <dest_ip> "
				"{-p|--port} <port> "
				"{-i|--iter} <iterations> "
				"{-s|--size} <packet_size> "
				"{-z|--trains} <train_length>\n", argv[0]);
		return 1;
	}

	if (sock_init(&sock, port, iface, daddr, 4) == -1) {
		fprintf(stderr, "Failed to initialize connection.\n");
		exit(1);
	}
	if (set_params(&sock, iter, size) == -1) {
		fprintf(stderr, "Failed to initialize receiver.\n");
		exit(1);
	}
	send_train(&sock, iter, size, trains);
	return 0;
}

int32_t sock_init(int32_t *sock, uint16_t port, char iface[16], char dst[40],
		uint8_t ipv) {
	void *sin;
	size_t sin_sz;
	struct sockaddr_in sin4 = {0};
	struct sockaddr_in6 sin6 = {0};

	switch(ipv) {
		case 4:
			sin4.sin_family = AF_INET;
			sin4.sin_port = htons(port);
			sin4.sin_addr.s_addr = inet_addr(dst);
			sin = &sin4;
			sin_sz = sizeof(sin4);
			*sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			break;
		case 6:
			sin6.sin6_family = AF_INET6;
			sin6.sin6_port = htons(port);
			inet_pton(AF_INET6, dst, &sin6.sin6_addr);
			sin = &sin6;
			sin_sz = sizeof(sin6);
			*sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
			break;
		default:
			return -1;
	}

	if (*sock == -1) {
		perror("socket");
		return -1;
	}

	if (setsockopt(*sock, SOL_SOCKET, SO_BINDTODEVICE, iface,
			sizeof(iface)) != 0) {
		perror("setsockopt (SO_BINDTODEVICE)");
		close(*sock);
		return -1;
	}

	if (connect(*sock, (struct sockaddr *) sin, sin_sz) == -1) {
		perror("connect");
		close(*sock);
		return -1;
	}
	return 0;
}

int32_t set_params(int32_t *sock, uint16_t iter, uint16_t size) {
	char buffer[size];
	struct msg *msg = (struct msg *) buffer;

	msg->type = MSG_CTRL;
	msg->data.c.iter = iter;
	msg->data.c.size = size;
	if (send(*sock, buffer, sizeof(buffer), 0) == -1) {
		perror("send");
		close(*sock);
		return -1;
	}
	return 0;
}

void send_train(int32_t *sock, uint16_t iter, uint16_t size, uint32_t train_length) {
	char buffer[size];
	uint64_t disp[iter];
	struct msg *msg = (struct msg *) buffer;
	struct timespec dispersion = {0}, delay = {.tv_sec = 0, .tv_nsec = 200000000};

	// initialize buffer and dispersion array
	memset(buffer, 0, size);
	memset(disp, 0, iter);

#ifdef LINEAR_DISPERSION
#ifndef D_MIN
#define D_MIN (size / (R_MIN / 1000000000.0))
#endif
#ifndef D_MAX
#define D_MAX (size / (R_MAX / 1000000000.0))
#endif
	// set constants for dispersion formula
	assert(iter != 0);
	// calculate dispersions to avoid calculations in the main loop
	for (uint32_t x = 0; x < iter; x++) {
		disp[x] = (D_MAX - D_MIN) / iter * x + D_MIN;
	}
#elif LINEAR_RATE
	assert(iter != 0);
	for (uint32_t x = 0; x < iter; x++) {
		disp[x] = (uint64_t) (size * 1000000000.0 / ((R_MAX - R_MIN) / iter
					* x + R_MIN));
	}
#endif
	msg->type = MSG_MSRMNT;
	for (uint32_t i = 0; i < train_length * iter; i++) {
		//memset(buffer, 0, sizeof(buffer));
		msg->data.m.iter = htons(i / train_length);  // 0 to (iter - 1)
		msg->data.m.id = i % train_length;  // 0 to (train_length - 1)
		clock_gettime(CLOCK_MONOTONIC_RAW, &msg->data.m.otime);
		// see send(2) and send(3)
		if (send(*sock, buffer, sizeof(buffer), 0) == -1) {
			perror("send");
			close(*sock);
			return;
		}
		/*
		 * Examples:
		 *
		 * packet size: 1000 Bytes
		 * speed: 768 kB/s (6000 kbit/s)
		 * => dispersion: 1271 us
		 *
		 * packet size: 1000 Bytes
		 * speed: 6.75 MB/s (54 Mbit/s)
		 * => dispersion: 212 us
		 *
		 * packet size: 1000 Bytes
		 * speed: 12.5 MB/s (100 Mbit/s)
		 * => dispersion: 76 us
		 *
		 * packet size: 1000 Bytes
		 * dispersion: 1 us
		 * => speed: 953 MB/s (7.45 Gbit/s)
		 */
		// see nanosleep(3)
		dispersion.tv_nsec = disp[i / train_length];
		nanosleep(&dispersion, NULL);
		if (i % train_length == train_length - 1) {
			// sleep between each packet pair
			nanosleep(&delay, NULL);
		}
	}

	close(*sock);
}
