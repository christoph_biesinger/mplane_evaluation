#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define handle_error(msg) \
	do {perror(msg); exit(EXIT_FAILURE);} while(0)

#define usage() \
	fprintf(stderr, "usage: %s " \
			"(-p|--port) <port>\n", argv[0])

int server_sock_init(uint16_t);
int serve(int);

int main(int argc, char *argv[]) {
	int sock = 0;
	uint8_t args = 0;
	uint16_t port = 0;
	int32_t c = 1, option_index = 0;
	struct option long_options[] = {
		{"bind", required_argument, 0, 'b'},
		{"port", required_argument, 0, 'p'}
	};

	while ((c = getopt_long(argc, argv, "p:", long_options,
					&option_index)) != -1) {
		switch (c) {
			case 'p':
				port = (uint16_t) atoi(optarg);
				args++;
				break;
			default:
				usage();
				exit(1);

		}
	}

	if (args != 1) {
		usage();
		exit(1);
	}

	sock = server_sock_init(port);
	serve(sock);
	close(sock);
	return EXIT_SUCCESS;
}

int server_sock_init(uint16_t port) {
	int sock = 0;
	struct sockaddr_in addr = {0};

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		handle_error("socket");
	}

	if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
		close(sock);
		handle_error("bind");
	}

	return sock;
}

int serve(int sock) {
	char *buf = NULL;
	int sock_client = 0;
	ssize_t bytes_sent = 0, bytes_received = 0, total = 0;
	uint32_t addr_len = 0, mtu = 0, mtu_len = 0;
	struct sockaddr_in addr = {0};

	if (listen(sock, 1) == -1) {
		close(sock);
		handle_error("listen");
	}

	addr_len = sizeof(addr);
	sock_client = accept(sock, (struct sockaddr *) & addr, &addr_len);
	if (sock_client == -1) {
		handle_error("accept");
	}

	// get MTU
	mtu_len = sizeof(mtu);
	getsockopt(sock_client, SOL_IP, IP_MTU, &mtu, &mtu_len);
	buf = malloc(mtu);
	if (buf == NULL) {
		close(sock_client);
		close(sock);
		handle_error("malloc");
	}

	/* send ~500 kiB */
	for (uint16_t i = 0; i < 512000 / mtu; i++) {
		bytes_sent = send(sock_client, buf, mtu, 0);
		if (bytes_sent == -1) {
			free(buf);
			close(sock_client);
			close(sock);
			handle_error("send");
		}
		total += bytes_sent;
	}
	printf("Bytes sent: %zd\n", total);

	puts("Waiting for ACK...");
	bytes_received = recv(sock_client, buf, mtu, 0);
	if (bytes_received > 0) {
		if (strncmp(buf, "ACK", 4) == 0) {
			puts("OK");
		} else {
			puts("Wrong ACK message");
			free(buf);
			close(sock_client);
			close(sock);
			exit(1);
		}
	} else if (bytes_received == 0) {
		puts("Couldn't receive ACK");
		free(buf);
		close(sock_client);
		close(sock);
		exit(1);
	} else if (bytes_received == -1)  {
		free(buf);
		close(sock_client);
		close(sock);
		handle_error("send");
	}

	total = 0;
	/* send ~5 MiB */
	for (uint16_t i = 0; i < 5242880 / mtu; i++) {
		bytes_sent = send(sock_client, buf, mtu, 0);
		if (bytes_sent == -1) {
			free(buf);
			close(sock_client);
			close(sock);
			handle_error("send");
		}
		total += bytes_sent;

	}
	close(sock_client);
	printf("Bytes sent: %zd\n", total);

	free(buf);
	return EXIT_SUCCESS;
}
