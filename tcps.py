"""
TCPS
TCP Server
Transmission Control Protocol

Python Wrapper fuer Server (bin/tcpserver)
"""

import time
import sys
import os.path as path
from subprocess import check_output
from subprocess import check_call

class tcps(object):

	def __init__(self, port):
		self.__Port = port
		"END"

	def run_measurementMethode(self):
		try:
			out = check_output(["./bin/tcpserver", "-p", self.__Port])
		except subprocess.CalledProcessError as e:
			print("./bin/tcpserver failed ")

		return out
		"END"

	def get_ACK(self, output):
		lines = output.splitlines()

		for line in lines:
			if line == 'OK':
				return True
		return False
		"END"

	"run Methode"
	def run(self):
		"Check if binary exists"
		if path.isfile("./bin/tcpserver") == False:
			"Create file"
			print("Create binarys")
			check_call(["make", "all"])

		print("start process: /bin/tcpserver")
		output = self.run_measurementMethode()
		"Extract Values"
		token = self.get_ACK(output)
		if token == True:
			print("Messung erfolgreich!")
		else:
			print("Fehlgeschlagen!")
		"END"

if __name__ == '__main__':
	port = "34000"

	tcp = tcps(port)
	tcp.run()
	"END"