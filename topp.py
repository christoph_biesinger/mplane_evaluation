"""
TOPP
Trains of Packet Pairs

Python Wrapper um Empfaenger-Messmethode (C-Datei /bin/receiver)

"""
import time
import sys
import os.path as path
from subprocess import check_output
from subprocess import check_call

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class topp(object):

	def __init__(self, device, port, timeout, trains):
		self.__Device = device
		self.__Port = port
		self.__Timeout = timeout
		self.__Trains = trains
		self.__CurrTime = time.strftime("%d-%m-%H-%M")
		self.__Filename = "TOPP_" + self.__CurrTime + "_" + \
						self.__Device + "_t" + self.__Trains
		"END"

	"run measurement in /bin/receiver"
	def run_measurementMethode(self):
		output = check_output(["./bin/receiver", "-b", \
			self.__Device, "-p", self.__Port, "-t", self.__Timeout, \
			"-z", self.__Trains])
		return output
		"END"

	"Extract measurement values and save in lists"
	def get_measurementValues(self, output):
		lines = output.splitlines()
		slist = []
		rlist = []

		for line in lines:
			if line != 'packages out of order':
				rvalue = line.split('receiving @'
					)[1].rsplit('(')[1].split(')')[0]
				svalue = line.split('receiving @'
					)[0].rsplit('(')[1].split(')')[0]
				rvalue = float(rvalue.strip(' ').split(' ')[0])
				svalue = float(svalue.strip(' ').split(' ')[0])
				rlist.append(rvalue)
				slist.append(svalue)
		return slist, rlist
		"END"

	"Save Data as Table"
	def save_Tables(self, slist, rlist):
		filename = "output/" + self.__Filename + ".out"
		np.savetxt(filename, (slist, rlist), fmt='%1.2f')
		"END"

	"Draw Graph with numpy"
	def draw_Graph(self, slist, rlist):
		filename = "output/" + self.__Filename + ".png"

		if (sys.version_info.major == 2):
			x = list(xrange(len(slist)))
		elif (sys.version_info.major == 3):
			x = list(range(len(slist)))
		else:
			return None

		"Line Graph - Values"
		point_s = plt.plot(x, slist, 'ro')
		point_r = plt.plot(x, rlist, 'bo')
		"Help Line"
		plt.plot(x, slist, 'r--')
		plt.plot(x, rlist, 'b--')
		"Grid"
		plt.grid(True)
		"Labels + Title"
		plt.xlabel('Iterations')
		plt.ylabel('Mbit/s')
		plt.legend((point_s[0], point_r[0]), ('Send', 'Receive'),
			loc='upper center', bbox_to_anchor=(0.5, -0.03),
			fancybox=True, shadow=True, ncol=2)
		"Save Graph"
		plt.savefig(filename)
		"END"

	"run methode"
	def run(self):
		"Check if binary exists"
		if path.isfile("./bin/receiver") == False:
			"Create file"
			print("Create binarys")
			check_call(["make", "all"])
		
		print("start process: /bin/receive")
		output = self.run_measurementMethode()
		"Extract Values"
		slist, rlist = self.get_measurementValues(output)
		"Draw Graph + Save Table"
		if ((isinstance(slist, list) == True) &
				(isinstance(rlist, list) == True)):
			self.draw_Graph(slist, rlist)
			self.save_Tables(slist, rlist)
		else:
			print("No Values!")
		"END"

if __name__ == '__main__':
	device = "enp0s25"
	port = "34000"
	timeout = "10"
	trains = "10"

	t = topp(device, port, timeout, trains)
	t.run()
	"END"
