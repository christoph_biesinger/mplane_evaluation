"""
TCPC
TCP Client
Transmission Control Protocol Download

Python Wrapper fuer TCP Client ()
"""
import time
import sys
import os.path as path
import subprocess
from subprocess import check_output
from subprocess import check_call

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class tcpc(object):

	def __init__(self, device, dest, port):
		self.__Device = device
		self.__Destination = dest
		self.__Port = port
		self.__CurrTime = time.strftime("%d-%m-%H-%M")
		self.__Filename = "TCPD" + self.__CurrTime + "_" + \
						self.__Device
		"END"

	"run measurement in /bin/receiver"
	def run_measurementMethode(self):

		#output = ""
		cmd = ["./bin/tcpclient", "-b", \
				self.__Device, "-d", self.__Destination, \
				"-p", self.__Port]
		output = subprocess.check_output(cmd, 
                shell=False, stderr=subprocess.STDOUT)
		"""
		try:
			print(command)

		except subprocess.CalledProcessError as e:
			print("./bin/tcpclient failed! %s%", e)
			return False
		"""
		"Check output"
		lines = output.splitlines()
		for line in lines:
			if "Connection refused" in str(line):
				print("Connection refused!")
				return False
		return output
		"END"

	"Extract measurement values and save in lists"
	def get_measurementValues(self, output):
		lines = output.splitlines()
		for line in lines:
			line = str(line)
		bytes = []
		btc = []

		for line in lines:
			line = str(line)
			if 'probe' in line:
				bytes_value = line.split('@')[0].split(']'
					)[1].strip(' ').split(' ')[0]
				btc_value = line.split('@')[1].strip(' '
					).split(' ')[0]
				bytes_value = int(bytes_value) / 1024
				btc_value = (float(btc_value) / 1000) * 8 
				bytes.append(bytes_value)
				btc.append(btc_value)
			if 'Bytes received' in line:
				bytes_value = line.split('@'
					)[0].split(' ')[2]
				btc_value = line.split('@')[1].strip(' '
					).split(' ')[0]
				bytes_value = int(bytes_value) / 1024
				btc_value = (float(btc_value) / 1000) * 8
				bytes.append(bytes_value)
				btc.append(btc_value)
		return bytes, btc
		"END"

	"Save Data as Table"
	def save_Tables(self, bytes, btc):
		filename = "output/" + self.__Filename + ".out"
		np.savetxt(filename, (bytes, btc), fmt='%1.2f')
		"END"

	"Draw Graph with numpy"
	def draw_Graph(self, bytes, btc):
		filename = "output/" + self.__Filename + ".png"

		if (sys.version_info.major == 2):
			x = list(xrange(len(btc)))
		elif (sys.version_info.major == 3):
			x = list(range(len(btc)))
		else:
			return None

		"Line Graph - Values"
		point = plt.plot(x, btc, 'ro')
		#bar = plt.bar(x, bytes)
		"Help Line"
		plt.plot(x, btc, 'r--')
		"Grid"
		plt.grid(True)
		"Labels + Title"
		plt.xlabel('Iterations')
		plt.ylabel('Mbit/s')
		"""plt.legend((point[0], bar[0]), ('BTC', 'Bytes'),
			loc='upper center', bbox_to_anchor=(0.5, -0.03),
			fancybox=True, shadow=True, ncol=2)
		"""
		"Save Graph"
		plt.savefig(filename)
		"END"

	"run methode"
	def run(self):
		"Check if binary exists"
		if path.isfile("./bin/tcpclient") == False:
			"Create file"
			print("Create binarys")
			check_call(["make", "all"])
		
		print("start process: /bin/tcpclient")
		output = self.run_measurementMethode()
		if output != False:
			"Extract Values"
			bytes, btc = self.get_measurementValues(output)
			"Draw Graph + Save Table"
			if (isinstance(btc, list) == True):
				self.draw_Graph(bytes, btc)
				self.save_Tables(bytes, btc)
			else:
				print("No Values!")
		else:
			print("Fehlgeschlagen!")
		"END"

if __name__ == '__main__':
	device = "wlp3s0"
	dest= '192.168.1.124'
	port = "34000"

	t = tcpc(device, dest, port)
	t.run()
	"END"
