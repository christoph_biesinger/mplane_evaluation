from subprocess import check_call

interface = 'wlp3s0'
dest_ip = '192.168.1.124'
port = '34000'
iterations = '50'
size = '100' # max 1458
trains = '10'

check_call(["./bin/sender", "-b", interface, "-d", dest_ip, \
	"-p", port, "-i", iterations, "-s", size, "-z", trains])

 
