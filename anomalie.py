"""
anomalie.py

"""
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time

class anomalie(object):

	def __init__(self, polling, polling_size, interrupt, 
		interrupt_size):
		self.__POLL = polling
		self.__sizePOLL = polling_size
		self.__INTER = interrupt
		self.__sizeINTER = interrupt_size
		"END"


	def draw_Graph(self, polling, polling_size, interrupt, 
		interrupt_size):

		filename = "Anomalie.png"

		"Line Graph - Values"
		point_poll = plt.plot(polling_size, polling, 'ro')
		point_inter = plt.plot(interrupt_size, interrupt, 'bo')

		"Help Line"
		plt.plot(polling_size, polling, 'r--')
		plt.plot(interrupt_size, interrupt, 'b--')
		"Grid"
		plt.grid(True)
		"Labels"
		plt.xlabel("Packet Size")
		plt.ylabel("Avg. Throughput")
		plt.legend( (point_poll[0], point_inter[0]), 
			('polling', 'interrupt'), loc='upper center',
			bbox_to_anchor=(0.5, 1.12), fancybox=True,
			shadow=True, ncol=4)
		"Save Graph"
		plt.savefig(filename)
		"END"

	def run(self):
		"Draw Graph"
		self.draw_Graph(self.__POLL, self.__sizePOLL, 
			self.__INTER, self.__sizeINTER)
		print("Erfolgreich")
		"END"

if __name__ == '__main__':
	polling = [(6.62 / 5.14), (9.98 / 5.60), (12.66 / 6.12), 
				(74.33/68.16)]
	polling_size = [50, 75, 100, 500 ]
	interrupt = [(), (), (), (), (), (), ()]
	interrupt_size = [50, 75, 100, 125, 150, 200, 500]
	a = anomalie(polling, polling_size, interrupt, interrupt_size)
	a.run()
	"END"