"""
VPS
Variable Packet size

Python Wrapper fuer VPS-Messmethode (C-Datei /bin/vps)
Messung: Kapazitaet Hops
"""
import time
import sys
import os.path as path
from subprocess import check_output
from subprocess import check_call

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class vps(object):

	def __init__(self, destination, size):
		self.__Destination = destination
		self.__Size = size
		self.__CurrTime = time.strftime("%d-%m-%H-%M")
		self.__Filename = "VPS_" + self.__CurrTime + "_" + \
						destination.replace('.', '-') + "_s" + \
						self.__Size
		"END"

	"run measurement in /bin/vps"
	def run_measurementMethode(self):
		output = check_output(["./bin/vps", "-d", \
				self.__Destination, "-s", self.__Size])
		return output
		"END"

	"Extract measurement values and save in lists"
	def get_measurementValues(self, output):
		lines = output.splitlines()
		for iteration in range(len(lines)):
			lines[iteration] = str(lines[iteration])
		values = []
		destinations = []

		"Remove dotted lines"
		for iteration in range(len(lines)):
			if '...' in lines[iteration]:
				lines.remove(iteration)

		for iteration in range(len(lines)):
			if ('ttl' in lines[iteration]) & \
					('dst' in lines[iteration]):
				line = lines[iteration].split(',')
				minValue = float((line[3].strip()).split(' ')[1])
				maxValue = float((line[4].strip()).split(' ')[1])
				avgValue = float((line[5].strip()).split(' ')[1])
				dst = (line[2].strip()).split(' ')[1]
				values.append([minValue, maxValue, avgValue])
				destinations.append(dst)
			else:
				values.append([0.0, 0.0, 0.0])
				destinations.append("Hop")
		return values, destinations
		"END"

	"Save Data as Table"
	def save_Tables(self, values, destinations):
		filename = "output/" + self.__Filename + ".out"
		minValues = []
		maxValues = []
		avgValues = []
		for value in values:
			minValues.append(value[0])
			maxValues.append(value[1])
			avgValues.append(value[2])
		np.savetxt(filename, (minValues, maxValues, \
					avgValues, destinations), \
					fmt='%s')
		"END"

	""
	def draw_Graph(self, values, destinations):
		filename = "output/" + self.__Filename + ".png"

		if (sys.version_info.major == 2):
			x = list(xrange(len(values)))
		elif (sys.version_info.major == 3):
			x = list(range(len(values)))
		else:
			return None
		"Values"
		minValues = []
		maxValues = []
		avgValues = []
		for value in values:
			minValues.append(value[0])
			maxValues.append(value[1])
			avgValues.append(value[2])
		"Line Graph - Values"
		point_min = plt.plot(x, minValues, 'ro')
		point_max = plt.plot(x, maxValues, 'bo')
		point_avg = plt.plot(x, avgValues, 'go')
		"xTicks - Destinations"
		plt.xticks(x, destinations, rotation='vertical')
		"Help Line"
		plt.plot(x, minValues, 'r--')
		plt.plot(x, maxValues, 'b--')
		plt.plot(x, avgValues, 'g--')
		"Grid"
		plt.grid(True)
		"Labels + Title"
		plt.xlabel('Destinations')
		plt.ylabel('Mbit/s')
		#plt.set_xticklabels(destinations)
		"Legende"
		plt.legend((point_min[0], point_max[0], point_avg[0]),
			('Minimal', 'Maximal', 'Average'),
			loc='upper center', bbox_to_anchor=(0.5, 1.12),
			fancybox=True, shadow=True, ncol=3)
		"Figure"
		fig = plt.gcf()
		fig.subplots_adjust(bottom=0.3)
		"Save Graph"
		plt.savefig(filename)
		"END"

	"run Methode"
	def run(self):
		"Check if binary exists"
		if path.isfile("./bin/vps") == False:
			"Create file"
			print("Create binarys")
			check_call(["make", "all"])

		print("start process: /bin/vps")
		output = self.run_measurementMethode()
		"Extract Values"
		values, destinations = self.get_measurementValues(output)
		"Draw Graph + Save Table"
		if ((isinstance(values, list) == True) &
				(isinstance(destinations, list) == True)):
			self.draw_Graph(values, destinations)
			self.save_Tables(values, destinations)
		else:
			print("No Values!")
		"END"

if __name__ == '__main__':
	destination = "141.82.51.236"
	size = "100"

	v = vps(destination, size)
	v.run()
	"END"
