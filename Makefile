CC = gcc
CFLAGS = -O2 --std=c99 -pedantic-errors -Wall -Wextra -Wno-missing-field-initializers
CPPFLAGS = -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L
#DEBUG = -g -DNDEBUG
CFLAGS += $(CPPFLAGS)
LDLIBS += -lrt

.PHONY: clean all

all: bin/sender bin/receiver bin/tcpserver bin/tcpclient bin/vps

clean:
	rm -f bin/*

bin/sender: src/sender.c src/topp.h
	$(CC) $(CFLAGS) $(DEBUG) -DLINEAR_RATE -o $@ $< $(LDLIBS)

bin/receiver: src/receiver.c src/topp.h
	$(CC) $(CFLAGS) $(DEBUG) -o $@ $< $(LDLIBS)

bin/tcpserver: src/tcpdl_server.c
	$(CC) $(CFLAGS) $(DEBUG) -o $@ $< $(LDLIBS)

bin/tcpclient: src/tcpdl_client.c
	$(CC) $(CFLAGS) $(DEBUG) -o $@ $< $(LDLIBS)

bin/vps: src/vps.c
	$(CC) $(CFLAGS) $(DEBUG) -o $@ $< $(LDLIBS)
