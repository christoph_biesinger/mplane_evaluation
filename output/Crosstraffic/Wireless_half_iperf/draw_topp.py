"""
draw_topp

2 Optionen

2 Parameter

draw_topp -o <graph|ratio> -f <filename>
"""
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
from optparse import OptionParser


class draw_topp(object):

	def __init__(self, option, filename):
		self.__Option = option
		self.__Filename = filename
		"END"

	def extract_measurementValues(self, filename):
		fobj = open(filename, 'r')
		lines = fobj.readlines()
		slist = lines[0].split(' ')
		rlist = lines[1].split(' ')
		return slist, rlist
		"END"

	def draw_Graph(self, slist, rlist):
		filename = self.__Filename.rstrip('.out') + ".png"

		if (sys.version_info.major == 2):
			x = list(xrange(len(slist)))
		elif (sys.version_info.major == 3):
			x = list(range(len(slist)))
		else:
			return None

		"Line Graph - Values"
		point_s = plt.plot(x, slist, 'ro')
		point_r = plt.plot(x, rlist, 'bo')
		"Help Line"
		plt.plot(x, slist, 'r--')
		plt.plot(x, rlist, 'b--')
		"Grid"
		plt.grid(True)
		"Labels + Title"
		plt.xlabel('Iterations')
		plt.ylabel('Mbit/s')
		plt.title(self.__Filename)
		plt.legend((point_s[0], point_r[0]), ('Send', 'Receive'),
			loc='upper center', bbox_to_anchor=(0.5, -0.03),
			fancybox=True, shadow=True, ncol=2)
		"Save Graph"
		plt.savefig(filename)
		"END"


	def draw_Ratio(self, slist, rlist):
		filename = self.__Filename.rstrip('.out') + "ratio.png"

		x = slist

		ratio = []
		for i in range(len(slist)):
			value = float(slist[i]) / float(rlist[i])
			ratio.append(value)
		"Line Graph - Values"
		point = plt.plot(x, ratio, 'ro')
		"Help Line"
		#plt.plot(x, ratio, 'r--')
		"Grid"
		plt.grid(True)
		"Labels"
		plt.xlabel('Offered probe rate')
		plt.ylabel('Offered / Measured probe rate')
		"Save Graph"
		plt.savefig(filename)
		"END"

	def run(self):
		"Extract Values from file"
		slist, rlist = self.extract_measurementValues(
			self.__Filename)

		"Draw Graph"
		if self.__Option == 'ratio':
			print("Draw Ratio")
			self.draw_Ratio(slist, rlist)
		else:
			print("Draw Graph")
			self.draw_Graph(slist, rlist)
		"END"


if __name__ == '__main__':

	"Extract given Parameters + Helpline"
	parser = OptionParser("draw_topp.py -o Option -f Filename")
	parser.add_option('-o', dest='option')
	parser.add_option('-f', dest='filename')
	(options, args) = parser.parse_args()

	option = options.option
	filename = options.filename

	topp = draw_topp(option, filename)
	topp.run()
	"END"


